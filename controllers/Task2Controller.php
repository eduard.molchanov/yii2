<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class Task2Controller extends Controller
{
    public function actionIndex()
    {
//        $model = new UserForm();
        $model = new User();

        if (Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash("success", "Данные сохранены");
        };

        return $this->render("index", compact("model"));
    }

    public function actionUpdate()
    {
        $model =User::findOne(8);
        if(!$model){
            throw  new NotAcceptableHttpException("User not found");
        }
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash("success", "Данные обновлены");
            return $this->refresh();
        };
        return $this->render("update", compact("model"));
    }
}