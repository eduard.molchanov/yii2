<?php

namespace app\controllers;

use app\models\User;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionIndex()
    {
//        $users = new User();
        $users = User::find()->all();

        return $this->render("index", compact("users"));
    }
}