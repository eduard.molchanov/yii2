<?php

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{

    public static function tableName()
    {
        return "users";
    }

    public function rules()
    {
        return [
            [["name", "email", "password"], "required"],
            ["email", "email"],
            [['email'], 'unique']
        ];
    }

    public function attributeLabels()
    {
        return [
            "name" => "Имя : ",
            "email" => "E-mail",
            "password" => "Пароль : "
        ];
    }
}