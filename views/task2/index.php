<?php

use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;

?>
<h2>Регистрация нового пользователя</h2>
<hr>

<div class="col-md-6">

    <?php $form = ActiveForm::begin([
        "id" => "new-user-form",
        "options" => [
            "class" => "form-horizontal",

            "fieldConfig" => [
                "template" => "{label} \n 
            <div class='col-md-5'> {input} </div> \n
            <div class='col-md-5'> {hint}  </div> \n
            <div class='col-md-5'> {error}  </div>",
                'labelOptions' => ['class' => 'col-md-2  control-label']
            ],
        ]
    ]) ?>

    <?= $form->field($model, "name")->input("text", ["placeholder" => "Введите имя"]) ?>
    <?= $form->field($model, "email", ['enableAjaxValidation' => true])->input("email", ["placeholder" => "Введите email"]) ?>

    <?= $form->field($model, "password")->passwordInput(["placeholder" => "Введите пароль"]) ?>
    <div class="form-group">
        <?= Html::submitButton("Зарегистрировать", ["class" => "btn btn-success"]) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>